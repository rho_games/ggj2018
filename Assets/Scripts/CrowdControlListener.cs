﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using rho;

public class CrowdControlListener : MonoBehaviour {

    [SerializeField] GameObject FanObject;

	// Use this for initialization
	void Start () {
        var bpm = PlayerData.CurrentBPMSelected;
        GlobalEventHandler.Register<CrowdMemberLeft>(OnPhaseEnd);
        PlayerData.RoundFansCollected = 
            bpm == 90 ? 40 : 
            bpm == 120 ? 60 :
            bpm == 150 ? 120 : 
            bpm == 300 ? 240 : 40;

        for (var i = 0; i < PlayerData.RoundFansCollected; i++)
        {
            // Create new fan around the unit circle
            var distanceFromCenter = new Vector2(
                Random.Range(0, 7),
                Random.Range(0, 3)
            );

            // Rotate our random Vector around a random amount
            var location = Quaternion.Euler(0, 0, Random.Range(0, 360f)) * distanceFromCenter;
            Instantiate(FanObject, location, Quaternion.identity);
        }
	}

    void OnPhaseEnd(CrowdMemberLeft evt)
    {
        PlayerData.RoundFansCollected--;
        if (PlayerData.RoundFansCollected <= 0)
        {
            SceneManager.LoadScene("GameOver");
        }
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
