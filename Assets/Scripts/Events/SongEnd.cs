﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rho;

public class SongEnd : IGameEvent 
{
    public int newBPM { get; set; }
}
