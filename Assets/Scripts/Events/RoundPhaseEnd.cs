﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rho;

// Result of the Phase.
public enum RoundPhaseResult
{
	Failure,
	Partial,
	Perfect
}

public class RoundPhaseEnded : IGameEvent
{
	// Percent Chance a fan will leave based off the outcome of the phase
	public static readonly Dictionary<RoundPhaseResult, float> fansLeavePercetnage =
		new Dictionary<RoundPhaseResult, float>
	{
		{RoundPhaseResult.Failure, 0.25f},
		{RoundPhaseResult.Partial, 0.15f},
		{RoundPhaseResult.Perfect, 0f}
	};

	// Should a Random Fan Leave (changes each call cause it's random)
	public bool RandomFanLeave
	{
		get
		{
			return Random.value < RoundPhaseEnded.fansLeavePercetnage[Result];
		}
	}

	public RoundPhaseResult Result;
}
