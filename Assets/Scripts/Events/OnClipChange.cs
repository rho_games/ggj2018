﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rho;

public enum ShapeList
{
    R = 0,
    G = 1,
    B = 2
}

public class OnClipChange : IGameEvent
{
    public ShapeList SelectedShape { get; set; }
    public bool IsOn { get; set; }
}