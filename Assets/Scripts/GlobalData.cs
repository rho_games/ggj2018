﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rho;

public class TotalFansCollectedChanged : rho.ValueChangeEvent<int>{}
public class RoundFansCollectedChanged : rho.ValueChangeEvent<int>{}

public static class PlayerData
{
	private static int _totalFansCollected = 0;
	// Number of Fans collected during the entire game
	// Only Changes at the end of the round
	public static int TotalFansCollected
	{
		get { return _totalFansCollected; }
		set
		{
			var evt = new TotalFansCollectedChanged {
				oldValue = _totalFansCollected,
				newValue = value
			};
			_totalFansCollected = value;
			// Trigger Event
			GlobalEventHandler.SendEvent(evt);
		}
	}

	private static int _roundFansCollected = 2;
	// Number of Fans collected during the current round
	// Reverts to 0 at the begining of each round
	public static int RoundFansCollected
	{
		get { return _roundFansCollected; }
		set
		{
			var evt = new RoundFansCollectedChanged {
				oldValue = _roundFansCollected,
				newValue = value
			};
			_roundFansCollected = value;
			// Trigger Event
			GlobalEventHandler.SendEvent(evt);
		}
	}

    public static int CurrentBPMSelected = 90;
    public static int CurrentRoundNumber = 1;
}
