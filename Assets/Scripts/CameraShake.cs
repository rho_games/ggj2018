﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rho;

public class CameraShake : MonoBehaviour
{
#region Shake

	[SerializeField]
	private float shakeTime        = 0.5f;
	private float elapsedShakeTime = 0.0f;

	[SerializeField]
	private float shakeEntisity = 0.1f;

	// Current shake offset
	private Vector3 cameraShakeOffset = Vector3.zero;

	private Camera _camera;

	private Vector3 _startPos;

	void Start()
	{
		_camera = GetComponent<Camera>();
	}

	public void StartShake()
	{
		if (elapsedShakeTime > 0) StopShake();
		elapsedShakeTime = shakeTime;
		cameraShakeOffset = Vector3.zero;
		_startPos = _camera.transform.position;
	}

	public void StopShake()
	{
		elapsedShakeTime = 0f;
		cameraShakeOffset = Vector3.zero;
		ApplyOffset();
	}

	private void ApplyOffset()
	{
		_camera.transform.position = _startPos + cameraShakeOffset;
	}

	private void Update()
	{
		if (elapsedShakeTime > 0)
		{
			cameraShakeOffset = Random.insideUnitSphere * Time.timeScale * shakeEntisity;
			cameraShakeOffset.z = 0f;

			elapsedShakeTime -= Time.deltaTime;

			if (elapsedShakeTime <= 0)
			{
				StopShake();
			}

			ApplyOffset();
		}
	}

#endregion
}
