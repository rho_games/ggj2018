﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rho;

public class ExitCollisionHandler : MonoBehaviour 
{
	void OnTriggerEnter2D(Collider2D collider)
	{
		if (collider.tag == "Fan")
		{
			Destroy(collider.gameObject);
		}
	}
}
