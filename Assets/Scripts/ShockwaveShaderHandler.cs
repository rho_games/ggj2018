﻿using UnityEngine;
using System.Collections;

internal struct ShockwaveFlyweight
{
	public bool active;
	public float elaspedTime;
	public Vector3 position;

	public void Reset( Vector3 pos )
	{
		this.position = pos;
		this.active = true;
		this.elaspedTime = 0.0f;
	}
}

// [ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
public class ShockwaveShaderHandler : MonoBehaviour 
{
	[SerializeField] private Shader shockWaveShader;

	[Range(0,1)]
	[SerializeField] private float Wavesize = 0.2f;

	[Range(-10,10)]
	[SerializeField] private float Amplitude = 0.05f;

	[SerializeField] private float targetCameraSize = 6.0f;

	private Camera cam;

	private Material material;

#region Shockwave Object Pool

	[Range(1, 50)]
	[SerializeField] private int maxShockwaves = 4;

	[SerializeField] private float shockwaveLifespan = 0.25f;

	private ShockwaveFlyweight[] shockWaves = new ShockwaveFlyweight[1];

	private float startingAmplitude = 0.0f;
	private float endingAmplitude = 0.2f;

	private float startingRadius = 0.0f;
	private float endingRadius = 0.176f;

#endregion

	void Start()
	{
		cam = this.GetComponent<Camera>();

		material = new Material(shockWaveShader);

		if ( ! cam )
		{
			Debug.LogWarning("Failed to Find Camera object in shockwave handler script");
		}

		// Allocate flyweights up front
		shockWaves = new ShockwaveFlyweight[ maxShockwaves ];

		for ( int i = 0 ; i < shockWaves.Length ; ++i )
		{
			shockWaves[ i ].position = new Vector3( 0f, 0f, 0f );
			shockWaves[ i ].active = false;
			shockWaves[ i ].elaspedTime = 0.0f;
		}
	}

	// Update is called once per frame
	void Update () 
	{
		if ( cam )
			material.SetFloat( "_Aspect", cam.aspect );

		// Update Shockwaves
		for ( int i = 0 ; i < shockWaves.Length ; ++i )
		{
			if ( ! shockWaves[ i ].active ) continue;

			shockWaves[ i ].elaspedTime += Time.deltaTime;

			if ( shockWaves[ i ].elaspedTime > shockwaveLifespan )
			{
				shockWaves[ i ].elaspedTime = 0;
				shockWaves[ i ].active = false;
			}
		}
	}

	public void AddNewShockwave( Vector2 position )
	{
		// Find first available shockwave, or shockwave with longest life, up to this point
		int index = getAvailableShockwaveIndex();

		shockWaves[ index ].Reset( new Vector3( position.x, position.y, 0.0f ) );
	}

	private int getAvailableShockwaveIndex()
	{
		float highestLife = -1;
		int highestLifeIndex = 0;

		for ( int i = 0 ; i < shockWaves.Length ; ++i )
		{
			if ( ! shockWaves[ i ].active )
				return i;

			// if this wave is the oldest one we've seen so far
			if ( shockWaves[ i ].elaspedTime > highestLife )
			{
				highestLife = shockWaves[ i ].elaspedTime;
				highestLifeIndex = i;
			}
		}

		return highestLifeIndex;
	}

	float getAmplitudeAtElapsedTime( float elapsed_time )
	{
		return ( elapsed_time / shockwaveLifespan ) * endingAmplitude + startingAmplitude;
	}

	float getRadiusAtElapsedTime( float elapsed_time )
	{
		return ( elapsed_time / shockwaveLifespan ) * endingRadius + startingRadius;
	}

	float getInverseRatioToCameraSize()
	{
		return targetCameraSize / cam.orthographicSize;
	}

	void OnRenderImage( Texture src, RenderTexture dest )
	{
		// Create temp render texture because Blit is lying to me and does something weird with the src or dest argument
		RenderTexture temp = RenderTexture.GetTemporary( cam.pixelWidth, cam.pixelHeight );
		RenderTexture temp2 = RenderTexture.GetTemporary( cam.pixelWidth, cam.pixelHeight );

		// Set temp as a copy of source so we start using temp as the source right away
		Graphics.Blit( src, temp );

		foreach ( ShockwaveFlyweight shockwave in shockWaves )
		{
			if ( ! shockwave.active ) continue;

			Vector3 viewport_pos = cam.WorldToViewportPoint( shockwave.position );   // converts world position to ( 0 -> 1, 0 -> 1 ) which is what shaders like

			material.SetFloat( "_CenterX", viewport_pos.x );
			material.SetFloat( "_CenterY", viewport_pos.y );
			material.SetFloat( "_Radius", getRadiusAtElapsedTime( shockwave.elaspedTime ) * getInverseRatioToCameraSize() );
			material.SetFloat( "_Wavesize", Wavesize * getInverseRatioToCameraSize() );
			material.SetFloat( "_Amplitude", Amplitude );   // get amplitude as a function of this shockwaves elapsed life

			Graphics.Blit( temp, temp2, material );	// copy contents of temp into temp, using material
			Graphics.Blit( temp2, temp );
		}

		Graphics.Blit( temp, dest );    // finaly copy temp into dest.  If no waves were rendered, then temp will just be src

		RenderTexture.ReleaseTemporary( temp2 );    // we don't need you anymore
		RenderTexture.ReleaseTemporary( temp );    // we don't need you anymore
	}
}
