﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rho;

// Starts the Music, does the countdown and then lets the beat drop
public class BeatDropHandler : MonoBehaviour
{
	public float _timeTillBeatDrop = 15f;
	bool _canDropBeat = false;
	AudioSource _audio;

	// Use this for initialization
	void Start()
	{
		_audio = GetComponent<AudioSource>();
		StartCoroutine(WaitForSongBuildUp());
	}

	IEnumerator WaitForSongBuildUp()
	{
		yield return new WaitForSeconds(_timeTillBeatDrop);
		_canDropBeat = true;
	}

	// Update is called once per frame
	void Update()
	{
		if (_canDropBeat && Input.GetKeyDown(KeyCode.Space))
		{
			GlobalEventHandler.SendEvent(new BeatDropEvent());
			_audio.Play();
		}
	}
}
