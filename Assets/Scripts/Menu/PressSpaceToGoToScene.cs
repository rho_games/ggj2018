﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rho;
using UnityEngine.SceneManagement;

public class PressSpaceToGoToScene : MonoBehaviour
{
	[SerializeField, Scene] string _scene;

	// Update is called once per frame
	void Update ()
	{
		if (Input.GetKeyDown(KeyCode.Space)) SceneManager.LoadScene(_scene);
	}
}
