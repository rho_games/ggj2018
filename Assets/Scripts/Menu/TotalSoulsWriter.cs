﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using rho;

public class TotalSoulsWriter : MonoBehaviour
{
	Text _text;

	// Use this for initialization
	void Start ()
	{
		_text = GetComponent<Text>();
	}

	// Update is called once per frame
	void Update ()
	{
		_text.text = PlayerData.TotalFansCollected.ToString();
	}
}
