﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using rho;

// Waits for Beat to be dropped then does a countup to all the new souls you
// have!
public class TotalSoulsDisplay : MonoBehaviour
{
	Animator _anim;
	// Time it takes to add all the souls
	[SerializeField] float _timeBeforeCountUp = 1f;
	[SerializeField] float _timeToCountUp = 5f;
	[SerializeField] float _timeTillSceneTransition = 5f;
	[SerializeField,Scene] string _nextScene;

	bool _doOnce = false;

	// Use this for initialization
	void Start()
	{
		_anim = GetComponent<Animator>();
		GlobalEventHandler.Register<BeatDropEvent>(OnBeatDrop);
	}

	void OnDestroy()
	{
		GlobalEventHandler.Unregister<BeatDropEvent>(OnBeatDrop);
	}

	void OnBeatDrop(BeatDropEvent evt)
	{
		if (_doOnce) return;

		_doOnce = true;
		_anim.SetTrigger("Start");
		StartCoroutine(CountUp());
	}

	IEnumerator CountUp()
	{
		yield return new WaitForSeconds(_timeBeforeCountUp);

		float increasePerFrame = PlayerData.RoundFansCollected / _timeToCountUp;
		int targetScore = PlayerData.TotalFansCollected + PlayerData.RoundFansCollected;

		while (PlayerData.TotalFansCollected < targetScore)
		{
			PlayerData.TotalFansCollected = Mathf.Min(
                PlayerData.TotalFansCollected + (int) Mathf.Ceil(increasePerFrame * Time.deltaTime),
				targetScore
			);
			yield return null; // wait for next frame
		}

		// Wait to go to next scene
		yield return new WaitForSeconds(_timeTillSceneTransition);

		SceneManager.LoadScene(_nextScene);
	}
}
