﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using rho;

public class GoToMainMenu : MonoBehaviour 
{
	[SerializeField] float _timeTillTransition = 8f;
	[SerializeField, Scene] string _scene;

	// Use this for initialization
	IEnumerator Start () 
	{
		yield return new WaitForSeconds(_timeTillTransition);

		SceneManager.LoadScene(_scene);
	}
}
