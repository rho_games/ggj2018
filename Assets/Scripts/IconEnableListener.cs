﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using rho;

public class IconEnableListener : MonoBehaviour {

    [SerializeField] private ShapeList keyLink;

    private Image imageInstance;
	// Use this for initialization
	void Start () {
        GlobalEventHandler.Register<OnClipChange>(EventChange);
        GlobalEventHandler.Register<ResetKeys>(ResetIcon);
        imageInstance = GetComponent<Image>();
        ResetIcon(new ResetKeys{ });
	}

    void EventChange(OnClipChange evt)
    {
        if (evt.SelectedShape == keyLink)
        {
            var col = imageInstance.color;
            col.a = evt.IsOn ? 1 : 0.5f;
            imageInstance.color = col;
        }
    }

    void ResetIcon(ResetKeys evt)
    {
        imageInstance.color = new Color(1, 1, 1, 0.5f);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
