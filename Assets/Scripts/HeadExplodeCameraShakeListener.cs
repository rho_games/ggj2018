﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rho;

public class HeadExplodeCameraShakeListener : MonoBehaviour
{
	CameraShake _shake;
	ShockwaveShaderHandler _shock;

	// Use this for initialization
	void Start ()
	{
		_shake = GetComponent<CameraShake>();
		_shock = GetComponent<ShockwaveShaderHandler>();
		GlobalEventHandler.Register<BeatDropEvent>(OnBeatDrop);
	}

	void OnDestroy()
	{
		GlobalEventHandler.Unregister<BeatDropEvent>(OnBeatDrop);
	}

	// Update is called once per frame
	void OnBeatDrop(BeatDropEvent evt)
	{
		_shake.StartShake();
		_shock.AddNewShockwave(transform.position);
	}
}
