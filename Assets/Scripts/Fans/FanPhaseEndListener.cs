﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rho;
using System.Linq;

// Detects when the Round Phase is over and if there is anything less than a
// perfect, this fan may choose to leave.  If they leave, find the nearest exit
// and to to it.
public class FanPhaseEndListener : MonoBehaviour
{
	private FanData _fanData;

	void Start()
	{
		GlobalEventHandler.Register<RoundPhaseEnded>(OnRoundPhaseEnded);
		_fanData = GetComponent<FanData>();
	}

	void OnDestroy()
	{
		GlobalEventHandler.Unregister<RoundPhaseEnded>(OnRoundPhaseEnded);
	}

	public void OnRoundPhaseEnded(RoundPhaseEnded evt)
	{
		// TODO Find a way to unregister during an event call
		if (_fanData.State == FanState.Leaving)
		{
			return;
		}

		if (evt.RandomFanLeave)
		{
			var exits = GameObject.FindGameObjectsWithTag("Exit").Select(x => x.transform).ToArray();

			Transform exit = null;

			if (exits.Any())
			{
				// Get Closest Exit
				foreach (var curr in exits)
				{
					if (exit == null)
					{
						exit = curr;
						break;
					}

					// Compare Distance to see which one is the closer
					if ((transform.position - curr.position).sqrMagnitude <
							(transform.position - exit.position).sqrMagnitude)
					{
						exit = curr;
					}
				}

				exit = exits[Random.Range(0, exits.Length)];
			}

			if (exit == null)
			{
				Debug.LogWarning("No Exits Found");
				return;
			}

			// Change state and make the Fan go to the exit
			_fanData.Leave(exit.position);
			GlobalEventHandler.SendEvent(new CrowdMemberLeft{});
		}
	}
}
