﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FanState
{
	Dancing             = 0,
	Shuffling           = 1,  // moving around dance floor
	MovingCloserToStage = 2,  // If the player is doing well, move closer to stage
	Leaving             = 3    // this place sucks, I'm leaving
}

// Fan State Data
public class FanData : MonoBehaviour
{
	public delegate void OnStateChange(FanState oldState, FanState newState);
	public event OnStateChange StateChanged = delegate {};
	private FanState _state = FanState.Dancing;

	public FanState State
	{
		get { return _state; }
		set
		{
			var oldState = _state;
			_state = value;
			StateChanged(oldState, _state);
		}
	}

	// Target the Player is currently walking towards, this means different
	// things based off current state
	public delegate void OnTargetChange(Vector2 newTarget);
	public event OnTargetChange TargetChanged = delegate {};
	public Vector2 _target = Vector2.zero;

	public Vector2 Target
	{
		get { return _target; }
		set
		{
			_target = value;
			TargetChanged(_target);
		}
	}

	/// <summary>
	/// Walk Speed of Fan
	/// </summary>
	public float WalkSpeed = 1.0f;


#region Helper Methods

	// Helper function because changing the state before target may screw up
	// people's responses
	public void Leave(Vector2 target)
	{
		Target = target;
		State = FanState.Leaving;
	}

#endregion
}
