﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Handles fan movement
public class FanMovement : MonoBehaviour
{
	private FanData _fanData;
	private IEnumerator _walkCoroutine = null;
	private Rigidbody2D _rigidBody;

	#region Events

	void Start()
	{
		_fanData = GetComponent<FanData>();
		_rigidBody = GetComponent<Rigidbody2D>();
		_fanData.StateChanged += OnStateChange;
	}

	void OnDestory()
	{
		_fanData.StateChanged -= OnStateChange;
	}

	#endregion

	IEnumerator WalkTowardsTarget()
	{
		while (Vector2.Distance(_rigidBody.position, _fanData.Target) > 0.005f)
		{
			var step = _fanData.WalkSpeed * Time.deltaTime;
			_rigidBody.MovePosition(
				Vector2.MoveTowards(_rigidBody.position, _fanData.Target, step)
			);
			yield return null;
		}

		// We've reached out target
		_fanData.Target = Vector3.zero;
		_fanData.State = FanState.Dancing;
	}

	public void OnStateChange(FanState oldState, FanState newState)
	{
		// Stop Coroutine
		// This may cause an issue if this is triggered from within our corutine
		if (_walkCoroutine != null)
		{
			StopCoroutine(_walkCoroutine);
			_walkCoroutine = null;
		}

		switch (newState)
		{
			case FanState.Dancing:
			{
				// Continue to Dance
				break;
			}
			case FanState.Shuffling:
			{
				break;
			}
			case FanState.MovingCloserToStage:
			{
				break;
			}
			case FanState.Leaving:
			{
				// Continue to move towards stage
				StartCoroutine(_walkCoroutine = WalkTowardsTarget());
				break;
			}
			default:
				break;
		}
	}
}
