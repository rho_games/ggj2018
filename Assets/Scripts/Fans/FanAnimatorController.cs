﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rho;

[System.Serializable]
public class FanSpriteInfo
{
	public Sprite[] _danceFrames = new Sprite[2];
	public Sprite _leavingSprite;
}

public class FanAnimatorController : MonoBehaviour
{
	FanData _fanData;
	SpriteRenderer _spriteRend;
	IEnumerator _danceCoroutine = null;

	float _frameTime = 60 / (float) PlayerData.CurrentBPMSelected;  // Beats Per Second
	int _currentDanceFrameIndex = 0;

	[Tooltip("A random config from this array will be chosen for each instance of this dancer")]
	public FanSpriteInfo[] _randomSpriteInfo;
	FanSpriteInfo _info;

	// Use this for initialization
	void Start()
	{
		_fanData = GetComponent<FanData>();
		_fanData.StateChanged += OnStateChange;

		_spriteRend = GetComponent<SpriteRenderer>();

		_info = _randomSpriteInfo[Random.Range(0, _randomSpriteInfo.Length)];

		OnStateChange(FanState.Dancing, _fanData.State);
	}

	void OnDestory()
	{
		_fanData.StateChanged -= OnStateChange;
	}

	// Swap Sprites and randomly flip
	IEnumerator Dance()
	{
		// Start on random frame
		_currentDanceFrameIndex = Random.Range(0, _info._danceFrames.Length);
		_spriteRend.sprite = _info._danceFrames[_currentDanceFrameIndex];

		while (true)
		{
			yield return new WaitForSeconds(_frameTime);
			_currentDanceFrameIndex = ( _currentDanceFrameIndex + 1 ) % _info._danceFrames.Length;
			_spriteRend.sprite = _info._danceFrames[_currentDanceFrameIndex];

			// Randomly decide to swap x too
			if (Random.value > 0.5f)
				_spriteRend.flipX = !_spriteRend.flipX;
		}
	}

	public void OnStateChange(FanState oldState, FanState newState)
	{
		if (_danceCoroutine != null)
		{
			StopCoroutine(_danceCoroutine);
			_danceCoroutine = null;
		}

		switch (newState)
		{
			case FanState.Dancing:
				// Start Dancing
				StartCoroutine(_danceCoroutine = Dance());
				break;
			case FanState.Leaving:
				_spriteRend.sprite = _info._leavingSprite;
				_spriteRend.flipX = _fanData.Target.x < transform.position.x;
				break;
			default:
				break;
		}
	}
}
