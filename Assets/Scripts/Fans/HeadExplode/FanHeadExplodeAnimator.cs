﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rho;

[System.Serializable]
public class FanExplodeSpriteInfo
{
	public Sprite _idle;
	public Sprite _explode;
}

// Listen for HeadExplosion
public class FanHeadExplodeAnimator : MonoBehaviour
{
	public FanExplodeSpriteInfo[] _randomSpriteInfo;
	private FanExplodeSpriteInfo _info;

	private SpriteRenderer _sprite;

	// Use this for initialization
	void Start()
	{
		GlobalEventHandler.Register<BeatDropEvent>(OnBeatDrop);

		_info = _randomSpriteInfo[Random.Range(0, _randomSpriteInfo.Length)];
		_sprite = GetComponent<SpriteRenderer>();

		_sprite.sprite = _info._idle;
	}

	// Update is called once per frame
	void OnDestory()
	{
		GlobalEventHandler.Unregister<BeatDropEvent>(OnBeatDrop);
	}

	public void OnBeatDrop(BeatDropEvent evt)
	{
		_sprite.sprite = _info._explode;
	}
}
