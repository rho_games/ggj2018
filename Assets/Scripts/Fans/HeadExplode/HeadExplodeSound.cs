﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rho;

public class HeadExplodeSound : MonoBehaviour
{
	AudioSource _audio;

	bool _doOnce = false;

	// Use this for initialization
	void Start ()
	{
		_audio = GetComponent<AudioSource>();
		GlobalEventHandler.Register<BeatDropEvent>(OnBeatDrop);
	}

	void OnDestroy()
	{
		GlobalEventHandler.Unregister<BeatDropEvent>(OnBeatDrop);
	}

	void OnBeatDrop(BeatDropEvent evt)
	{
		if (_doOnce) return;

		_doOnce = true;
		_audio.Play();
	}
}
