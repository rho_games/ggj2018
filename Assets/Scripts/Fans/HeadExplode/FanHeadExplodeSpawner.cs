﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rho;

// Spawn Fans in circle around truck
public class FanHeadExplodeSpawner : MonoBehaviour
{
	[SerializeField] GameObject _prefab;
	[SerializeField] float _minDistanceFromCenter = 5f;
	float _maxDistanceFromCenter = 20f;

	// Use this for initialization
	void Start ()
	{
		_maxDistanceFromCenter = _minDistanceFromCenter +
			//0;
			PlayerData.RoundFansCollected * 0.01f;

		for (int i = 0 ; i < PlayerData.RoundFansCollected; ++i)
		{
			// Create new fan around the unit circle
			var distanceFromCenter = new Vector2(
				Random.Range(_minDistanceFromCenter, _maxDistanceFromCenter),
				Random.Range(_minDistanceFromCenter, _maxDistanceFromCenter)
			);

			// Rotate our random Vector around a random amount
			var location = Quaternion.Euler(0, 0, Random.Range(0, 360f)) * distanceFromCenter;
			var obj = Instantiate(_prefab, location, Quaternion.identity);

			// Flip if we are left of the truck
			var sprite = obj.GetComponent<SpriteRenderer>();
			sprite.flipX = location.x > 0;
		}
	}
}
