﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rho;

// This is garbage and shouldn't be included in the game
public class FanTester : MonoBehaviour
{
	private FanData _fanData;
	public Transform _target;

	void Start()
	{
		_fanData = GetComponent<FanData>();
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Space))
		{
			GlobalEventHandler.SendEvent(new RoundPhaseEnded{ Result = RoundPhaseResult.Failure });
		}
	}
}
