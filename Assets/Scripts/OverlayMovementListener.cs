﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using rho;

public class OverlayMovementListener : MonoBehaviour {

    [SerializeField] private Image Icon1;
    [SerializeField] private Image Icon2;
    [SerializeField] private Image Icon3;
    [SerializeField] private Sprite DrumIcon;
    [SerializeField] private Sprite BassIcon;
    [SerializeField] private Sprite LeadIcon;

    [SerializeField] private Image LineImage;



    private float FallingRate;
    private RectTransform tmpRect;

	// Use this for initialization
	void Start () {
        tmpRect = GetComponent<RectTransform>();

        FallingRate = PlayerData.CurrentBPMSelected == 90 ? 180 :
            PlayerData.CurrentBPMSelected == 120 ? 270 :
            PlayerData.CurrentBPMSelected == 150 ? 540 :
            PlayerData.CurrentBPMSelected == 300 ? 1080 : 180;
        GlobalEventHandler.Register<OnNewInstrumentSet>(EventChange);
	}

    void EventChange(OnNewInstrumentSet evt)
    {
        var curPos = tmpRect.anchoredPosition;
        curPos.y = 0;
        tmpRect.anchoredPosition = curPos;
        int startingIcon = 1;
        Icon1.sprite = evt.RedPressed ? DrumIcon : null;
        Icon2.sprite = evt.GreenPressed ? BassIcon : null;
        Icon3.sprite = evt.BluePressed ? LeadIcon : null;

        Icon1.color = new Color(1, 1, 1, Icon1.sprite == null ? 0 : 1);
        Icon2.color = new Color(1, 1, 1, Icon2.sprite == null ? 0 : 1);
        Icon3.color = new Color(1, 1, 1, Icon3.sprite == null ? 0 : 1);

        LineImage.color = new Color(evt.RedPressed ? 1 : 0, evt.GreenPressed ? 1 : 0, evt.BluePressed ? 1 : 0, 0.40f);
    }
	
	// Update is called once per frame
	void Update () {
        var curPos = tmpRect.anchoredPosition;
        curPos.y -= (Time.deltaTime * FallingRate);
        tmpRect.anchoredPosition = curPos;
	}
}
