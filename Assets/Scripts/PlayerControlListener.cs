﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rho;

public enum KeyPressed
{
    A = 0,
    S = 1,
    D = 2,
    Spc = 3
}

public class OnPlayerKeyPress : IGameEvent
{
    public KeyPressed key { get; set; }
    public bool isPressed { get; set; }
}

public class PlayerControlListener : MonoBehaviour {
	
	// Update is called once per frame
	void Update () 
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            GlobalEventHandler.SendEvent(new OnPlayerKeyPress { key = KeyPressed.A, isPressed = true });
        }
        if (Input.GetKeyDown(KeyCode.S))
        {
            GlobalEventHandler.SendEvent(new OnPlayerKeyPress { key = KeyPressed.S, isPressed = true  });
        }
        if (Input.GetKeyDown(KeyCode.D))
        {
            GlobalEventHandler.SendEvent(new OnPlayerKeyPress{ key = KeyPressed.D, isPressed = true  });
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            GlobalEventHandler.SendEvent(new OnPlayerKeyPress { key = KeyPressed.A, isPressed = false });
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            GlobalEventHandler.SendEvent(new OnPlayerKeyPress { key = KeyPressed.S, isPressed = false });
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            GlobalEventHandler.SendEvent(new OnPlayerKeyPress{ key = KeyPressed.D, isPressed = false });
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GlobalEventHandler.SendEvent(new OnPlayerKeyPress{ key = KeyPressed.Spc });
        }
	}
}
