﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using rho;

public class SelectedColorListener : MonoBehaviour {

    private bool redPressed = false;
    private bool greenPressed = false;
    private bool bluePressed = false;

    private Image imageInstance;

	// Use this for initialization
	void Start () {
        imageInstance = GetComponent<Image>();
        GlobalEventHandler.Register<OnClipChange>(EventChange);
        ChangeShapeColor();
	}

    void EventChange(OnClipChange evt)
    {
        if (evt.SelectedShape == ShapeList.R)
            redPressed = evt.IsOn;
        if (evt.SelectedShape == ShapeList.G)
            greenPressed = evt.IsOn;
        if (evt.SelectedShape == ShapeList.B)
            bluePressed = evt.IsOn;
        ChangeShapeColor();
    }

    void ChangeShapeColor()
    {
        imageInstance.color = new Color(redPressed ? 1 : 0, greenPressed ? 1 : 0, bluePressed ? 1 : 0);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
