﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using rho;

public class OnColorTransition : IGameEvent
{
    public Color newColor { get; set; }
}

public class ColorChangeListener : MonoBehaviour {

    private Color currentColor = Color.white;
    private Color nextColor = Color.white;
    private Color debugColor = Color.white;

    private float transitionRate;
    private float elapsedTransitionTime = 0;
    private float debugCurrentLerpPercentage = 0;

    private bool isChanging = false;

    private Image imageInstance;

	// Use this for initialization
	void Start () {
        transitionRate = 
            PlayerData.CurrentBPMSelected == 90 ? 3 : 
            PlayerData.CurrentBPMSelected == 120 ? 2 : 
            PlayerData.CurrentBPMSelected == 150 ? 1 : 
            PlayerData.CurrentBPMSelected == 300 ? 0.5f : 3;

        imageInstance = GetComponent<Image>();

        GlobalEventHandler.Register<OnColorTransition>(ChangeEvent);
	}

    void ChangeEvent(OnColorTransition evt)
    {
        isChanging = true;
        elapsedTransitionTime = 0f;
        currentColor = nextColor;
        nextColor = evt.newColor;
        isChanging = false;
    }
	
	// Update is called once per frame
	void Update () {
        if (!isChanging)
        {
            elapsedTransitionTime += Time.deltaTime;
            debugCurrentLerpPercentage = elapsedTransitionTime / transitionRate;
            debugColor = Color.Lerp(currentColor, nextColor, debugCurrentLerpPercentage);
            imageInstance.color = debugColor;
        }
	}
}
