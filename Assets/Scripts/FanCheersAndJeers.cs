﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rho;

// Play a sound based off the outcome of the Phase
public class FanCheersAndJeers : MonoBehaviour
{
	AudioSource _audio;

	[SerializeField] AudioClip[] _jeers;
	[SerializeField] AudioClip[] _cheers;

	// Use this for initialization
	void Start()
	{
		_audio = GetComponent<AudioSource>();
		GlobalEventHandler.Register<RoundPhaseEnded>(OnRoundPhaseEnd);
	}

	void OnDestroy()
	{
		GlobalEventHandler.Unregister<RoundPhaseEnded>(OnRoundPhaseEnd);
	}

	void OnRoundPhaseEnd(RoundPhaseEnded evt)
	{
		// Get Clip Array based off responses
		var clipArray = evt.Result == RoundPhaseResult.Perfect ? _cheers : _jeers;

		if (clipArray == null || clipArray.Length < 1) return;

		_audio.PlayOneShot(clipArray[Random.Range(0, clipArray.Length)]);
	}
}
