﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using rho;

public class CurrentBeatSelection
{
    public bool isRed { get; set; }
    public bool isGreen { get; set; }
    public bool isBlue { get; set; }
}

public class ResetKeys : IGameEvent
{}

public class OnNewInstrumentSet : IGameEvent
{
    public bool RedPressed { get; set; }
    public bool GreenPressed { get; set; }
    public bool BluePressed { get; set; }
}

public class GameRoundManager : MonoBehaviour {

    private float lengthOfBeatDelay;
    private float currentBeatDelayPosition;
    private float currentStartingRoundDelay;

    private bool redPressed = false;
    private bool greenPressed = false;
    private bool bluePressed = false;

    private List<CurrentBeatSelection> BeatQueue;

    [SerializeField] private Text CountdownTextbox;

	// Use this for initialization
	void Start () {
        currentBeatDelayPosition = 0;
        lengthOfBeatDelay = 
            PlayerData.CurrentBPMSelected == 90 ? 0.33f : 
            PlayerData.CurrentBPMSelected == 120 ? 0.5f : 
            PlayerData.CurrentBPMSelected == 150 ? 1 : 
            PlayerData.CurrentBPMSelected == 300 ? 2 : 0.33f;

        var totalBeatItems = 
            PlayerData.CurrentBPMSelected == 90 ? 20 : 
            PlayerData.CurrentBPMSelected == 120 ? 30 : 
            PlayerData.CurrentBPMSelected == 150 ? 60 : 
            PlayerData.CurrentBPMSelected == 300 ? 120 : 20;

        BeatQueue = new List<CurrentBeatSelection>();

        for (var i = 0; i < totalBeatItems; i++)
        {
            var queueItem = new CurrentBeatSelection{ isRed = Random.Range(0, 100) > 50, isGreen = Random.Range(0, 100) > 50, isBlue = Random.Range(0, 100) > 50 };
            if (!queueItem.isRed && !queueItem.isGreen && !queueItem.isBlue)
            {
                var rando = Random.Range(0, 3);
                queueItem.isRed = rando == 0;
                queueItem.isGreen = rando == 1;
                queueItem.isBlue = rando == 2;
            }
            BeatQueue.Add(queueItem);
        }

        GlobalEventHandler.Register<OnPlayerKeyPress>(PlayerReaction);
        GlobalEventHandler.SendEvent(new ResetKeys{ });
	}

    void SendNewColor()
    {
        var colorToSend = new Color(BeatQueue[0].isRed ? 1 : 0, BeatQueue[0].isGreen ? 1 : 0, BeatQueue[0].isBlue ? 1 : 0);
        GlobalEventHandler.SendEvent(new OnColorTransition { newColor = colorToSend });
        GlobalEventHandler.SendEvent(new OnNewInstrumentSet { RedPressed = BeatQueue[0].isRed, GreenPressed = BeatQueue[0].isGreen, BluePressed = BeatQueue[0].isBlue });
    }
	
	// Update is called once per frame
	void Update () {
        if (currentStartingRoundDelay < 6)
        {
            currentStartingRoundDelay += Time.deltaTime;
            CountdownTextbox.text = (6 - (int)currentStartingRoundDelay).ToString();
        }
        if (currentStartingRoundDelay >= 6)
        {
            if (CountdownTextbox.enabled)
            {
                SendNewColor();
            }
            CountdownTextbox.enabled = false;
            currentBeatDelayPosition += Time.deltaTime * lengthOfBeatDelay;
            if (currentBeatDelayPosition >= 1)
            {
                currentBeatDelayPosition = 0;
                if (BeatQueue.Count == 1)
                {
                    PlayerData.CurrentRoundNumber++;
                    SceneManager.LoadScene("TruckScene");
                }
                else
                {
                    var newVal = (BeatQueue[0].isRed == redPressed ? 0 : 1) + (BeatQueue[0].isGreen == greenPressed ? 0 : 1) + (BeatQueue[0].isBlue == bluePressed ? 0 : 1);
                    GlobalEventHandler.SendEvent(new RoundPhaseEnded { Result = newVal == 0 ? RoundPhaseResult.Perfect : newVal == 3 ? RoundPhaseResult.Failure : RoundPhaseResult.Partial });
                    BeatQueue.RemoveAt(0);
                }

                SendNewColor();
            }
        }
	}

    void PlayerReaction(OnPlayerKeyPress evt)
    {
        if (evt.key == KeyPressed.A)
        {
            redPressed = evt.isPressed;
            GlobalEventHandler.SendEvent(new OnClipChange{ SelectedShape = ShapeList.R, IsOn = redPressed });
        }
        if (evt.key == KeyPressed.S)
        {
            greenPressed = evt.isPressed;
            GlobalEventHandler.SendEvent(new OnClipChange{ SelectedShape = ShapeList.G, IsOn = greenPressed });
        }
        if (evt.key == KeyPressed.D)
        {
            bluePressed = evt.isPressed;
            GlobalEventHandler.SendEvent(new OnClipChange{ SelectedShape = ShapeList.B, IsOn = bluePressed });
        }
    }

    void Awake()
    {
        PlayerData.CurrentBPMSelected =
            PlayerData.CurrentRoundNumber == 1 ? 90 :
            PlayerData.CurrentRoundNumber == 2 ? 120 :
            PlayerData.CurrentRoundNumber == 3 ? 150 :
            PlayerData.CurrentRoundNumber == 4 ? 300 : 90;

        if (PlayerData.CurrentRoundNumber > 4)
        {
            var randomBPM = Random.Range(0, 4);
            PlayerData.CurrentBPMSelected = 
                randomBPM == 0 ? 90 :
                randomBPM == 1 ? 120 :
                randomBPM == 2 ? 150 : 
                randomBPM == 3 ? 300 : 90;
        }
    }
}
