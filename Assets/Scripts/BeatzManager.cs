﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rho;

public class BeatzManager : MonoBehaviour {

    [Header("90BPM Clips")]
    [SerializeField] List<AudioClip> R90Clips;
    [SerializeField] List<AudioClip> G90Clips;
    [SerializeField] List<AudioClip> B90Clips;

    [Header("120BPM Clips")]
    [SerializeField] List<AudioClip> R120Clips;
    [SerializeField] List<AudioClip> G120Clips;
    [SerializeField] List<AudioClip> B120Clips;

    [Header("150BPM Clips")]
    [SerializeField] List<AudioClip> R150Clips;
    [SerializeField] List<AudioClip> G150Clips;
    [SerializeField] List<AudioClip> B150Clips;

    [Header("300BPM Clips")]
    [SerializeField] List<AudioClip> R300Clips;
    [SerializeField] List<AudioClip> G300Clips;
    [SerializeField] List<AudioClip> B300Clips;

    [Header("Audio Source Links")]
    [SerializeField] AudioSource R1Source;
    [SerializeField] AudioSource R2Source;
    [SerializeField] AudioSource R3Source;
    [SerializeField] AudioSource G1Source;
    [SerializeField] AudioSource G2Source;
    [SerializeField] AudioSource G3Source;
    [SerializeField] AudioSource B1Source;
    [SerializeField] AudioSource B2Source;
    [SerializeField] AudioSource B3Source;

    [Header("Beat Drums")]
    [SerializeField] AudioSource BeatSource;
    [SerializeField] AudioClip BPM90Beat;
    [SerializeField] AudioClip BPM120Beat;
    [SerializeField] AudioClip BPM150Beat;
    [SerializeField] AudioClip BPM300Beat;

    private int CurrentRClip = 0;
    private int CurrentGClip = 0;
    private int CurrentBClip = 0;

    private bool ROn = false;
    private bool GOn = false;
    private bool BOn = false;

	// Use this for initialization
	void Start () {
        RefreshMusicSources();
        GlobalEventHandler.Register<OnClipChange>(ChangeEvent);
	}

    void RefreshMusicSources()
    {
        int currentBPM = PlayerData.CurrentBPMSelected;

        var RSelected = currentBPM == 120 ? R120Clips : currentBPM == 150 ? R150Clips : currentBPM == 300 ? R300Clips : R90Clips;
        var GSelected = currentBPM == 120 ? G120Clips : currentBPM == 150 ? G150Clips : currentBPM == 300 ? G300Clips : G90Clips;
        var BSelected = currentBPM == 120 ? B120Clips : currentBPM == 150 ? B150Clips : currentBPM == 300 ? B300Clips : B90Clips;

        R1Source.clip = RSelected[0];
        R2Source.clip = RSelected[1];
        R3Source.clip = RSelected[2];
        G1Source.clip = GSelected[0];
        G2Source.clip = GSelected[1];
        G3Source.clip = GSelected[2];
        B1Source.clip = BSelected[0];
        B2Source.clip = BSelected[1];
        B3Source.clip = BSelected[2];

        BeatSource.clip = 
            currentBPM == 90 ? BPM90Beat :
            currentBPM == 120 ? BPM120Beat : 
            currentBPM == 150 ? BPM150Beat :
            currentBPM == 300 ? BPM300Beat : BPM90Beat;

        R1Source.Play();
        R2Source.Play();
        R3Source.Play();
        G1Source.Play();
        G2Source.Play();
        G3Source.Play();
        B1Source.Play();
        B2Source.Play();
        B3Source.Play();
        BeatSource.Play();

        R1Source.mute = false;
    }

    public void OnDestroy()
    {
        GlobalEventHandler.Unregister<OnClipChange>(ChangeEvent);
    }

    public void ChangeEvent(OnClipChange evt)
    {
        var val = evt;

        switch (val.SelectedShape)
        {
            case ShapeList.R:
                R1Source.mute = CurrentRClip != 0 || !val.IsOn;
                R2Source.mute = CurrentRClip != 1 || !val.IsOn;
                R3Source.mute = CurrentRClip != 2 || !val.IsOn;

                CurrentRClip = (CurrentRClip + 1) % 3;
                break;
            case ShapeList.G:
                G1Source.mute = CurrentGClip != 0 || !val.IsOn;
                G2Source.mute = CurrentGClip != 1 || !val.IsOn;
                G3Source.mute = CurrentGClip != 2 || !val.IsOn;

                CurrentGClip = (CurrentGClip + 1) % 3;
                break;
            case ShapeList.B:
                B1Source.mute = CurrentBClip != 0 || !val.IsOn;
                B2Source.mute = CurrentBClip != 1 || !val.IsOn;
                B3Source.mute = CurrentBClip != 2 || !val.IsOn;

                CurrentBClip = (CurrentBClip + 1) % 3;
                break;
        }
    }
}
