﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using rho;

public class ResetAllPlayerData : MonoBehaviour
{
	// Use this for initialization
	void Start ()
	{
		PlayerData.TotalFansCollected = 0;
		PlayerData.RoundFansCollected = 0;
		PlayerData.CurrentBPMSelected = 90;
		PlayerData.CurrentRoundNumber = 1;
	}
}
